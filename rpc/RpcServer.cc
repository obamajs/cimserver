#include "RpcServer.h"

RpcServer::RpcServer(string host, short port, AsyncDispatcher* dispatcher) :BaseTcpServer(host, port, dispatcher) {}
RpcServer::~RpcServer() {}

TcpSession* RpcServer::createSession() {
    return new RpcSession(this);
}

void RpcServer::RegisterService(std::shared_ptr<RpcService>service) {
    services.push_back(service);
    std::map<std::string, shared_ptr<service_method_descriptor>>descriprtors = service->GetDescriptors();
    for (std::map<std::string, shared_ptr<service_method_descriptor>>::iterator it = descriprtors.begin();it != descriprtors.end();it++) {
        service_descriptors[it->first] = it->second;
    }
}

shared_ptr<service_method_descriptor>RpcServer::QueryHandler(const string& method) {
    std::map<std::string, shared_ptr<service_method_descriptor>>::iterator result = service_descriptors.find(method);
    if (result != service_descriptors.end()) {
        return result->second;
    }
    else {
        return nullptr;
    }
}